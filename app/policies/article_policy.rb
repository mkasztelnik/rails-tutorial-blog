class ArticlePolicy < ApplicationPolicy
  class Scope
    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      @scope.where(status: :published).or(Article.where(user: @user))
    end
  end

  def index?
    user
  end

  def create?
    user
  end

  def show?
    record.user == user || record.published?
  end

  def edit?
    record.user == user && record.draft?
  end

  def update?
    record.user == user && record.draft?
  end

  def destroy?
    record.user == user && record.draft?
  end
end
