class ArticleIndexerJob < ApplicationJob
  queue_as :default

  def perform(article)
    sleep(10)
    Rails.logger.info("Indexer mock invoked after 10 seconds for #{article.title}")

    if article.published?
      NotificationChannel.
        broadcast_to('notification',
                     msg: "New article published: #{article.title}",
                     status: 'success')
    end
  end
end
