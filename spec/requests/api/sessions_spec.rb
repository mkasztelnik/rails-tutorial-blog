require 'rails_helper'

RSpec.describe Api::SessionsController do
  it 'generates token after successful login' do
    user = create(:user, password: 'password')

    post api_sessions_path,
      params: { user: { email: user.email, password: 'password' } }

    expect(response.status).to eq 201
    data = JSON.parse(response.body)
    expect(data['user']['email']).to eq user.email
    expect(User.from_token(data['user']['token'])).to eq user
  end

  it 'returns 401 when email/password combination is wrong' do
    post api_sessions_path,
      params: { user: { email: 'a@b.pl', password: 'wrong' } }

    expect(response.status).to eq 401
  end
end
