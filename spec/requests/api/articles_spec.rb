require 'rails_helper'

RSpec.describe Api::ArticlesController do
  context 'as non logged in user' do
    it 'there is no possibility to get articles list' do
      get api_articles_path

      expect(response.status).to eq 401
    end

    # other tests for articles api can be created here
  end

  context 'as a logged in user' do
    let(:user) { create(:user) }
    let(:token) { user.token }

    it 'I can list my and published articles' do
      create(:article, user: user)
      create(:article, title: 'published', status: :published)
      create(:article, title: 'not-visible', status: :draft)

      get api_articles_path,
        headers: { 'Authorization': "Bearer #{token}",
                   'Accept': 'application/json' }

      expect(response.status).to eq 200
      data = JSON.parse(response.body)
      expect(data['articles'].size).to eq 2
    end

    # Other tests for post/put/delete
  end
end
