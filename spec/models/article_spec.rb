require 'rails_helper'

RSpec.describe Article do
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:text) }
  it { should belong_to(:user) }

  context 'slugable' do
    it 'create slug after record is stored in db' do
      article = create(:article, title: 'My new article')

      expect(article.slug).to eq 'my-new-article'
    end
  end

  context '#publishing?' do
    it 'is true when changing status from draft to published' do
      article = create(:article, status: :draft)

      article.update(status: :published)

      expect(article.publishing?).to be_truthy
    end

    it 'is false when article is draft' do
      article = create(:article, status: :draft)

      article.update(status: :draft)

      expect(article.publishing?).to be_falsy
    end

    it 'is false when article is already published' do
      article = create(:article, status: :published)

      article.update(status: :published)

      expect(article.publishing?).to be_falsy
    end
  end
end
