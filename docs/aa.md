# Authentication and authorization

## Authentication using Devise

Add into `Gemfile`

```ruby
gem 'devise'
```

and run:

```
bundle install
rails generate devise:install
rails restart
```

edit `config/environments/development.rb` and add:

```ruby
config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
```

add:

```ruby
config.secret_key = Rails.application.credentials.secret_key_base
```

into `Devise.setup` section into `config/initializers/devise.rb` file and run:

```
rails generate devise User
rails db:migrate
```

Add following code into `app/controllerts/application_controller.rb`:

```ruby
class ApplicationController < ActionController::Base
  before_action :authenticate_user!
end
```

To "unprotect" `welcome#index` add following code into
`app/controllerts/welcome_controller.rb`:

```
skip_before_action :authenticate_user!
```

Lets add login links and user info into `welcome#index`. To do this change
`app/views/welcome/index.html.haml` to:

```haml
%h1= t '.title'
.alert.alert-primary
  = icon 'calendar'
  = @now

- if user_signed_in?
  %p
    Welcome
    = current_user.email
  %small= link_to 'Logout', destroy_user_session_path,
    method: :delete, class: 'btn btn-danger'
- else
  = link_to 'Log in', new_user_session_path, class: 'btn btn-success'

```

To show flash messages create `apps/views/layouts/_flash.html.haml` with
following content:

```haml
- if alert
  .alert.alert-danger.alert-dismissible.fade.show{role: 'alert'}
    = alert
    %button.close{type: 'button', data: { dismiss: 'alert' }}
      %span{"aria-hidden" => "true"} &times;

- elsif notice
  .alert.alert-primary.alert-dismissible.fade.show{role: 'alert'}
    = notice
    %button.close{type: 'button', data: { dismiss: 'alert' }}
      %span{"aria-hidden" => "true"} &times;
```

And render it in `app/views/layouts/application.html.haml`

```html
!!! 5
%html
  = render 'layouts/head'
  %body
    %main.container{role: 'main'}
      = render 'layouts/flash'
      = yield
```

* Explain how views can be customized (`rails generate devise:views`)
* Explain how to customize controller (`rails generate devise:controllers users`)
* Show `devise_for` in `config/routes.rb`
* Mention about I18n
* Explain how `flash` works

## Omniauth (login using FB and G+)

Add 2 new depedencies into `Gemfile`:

```ruby
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
```

run:

```
bundle install
```

Add configuration for FB and G OAuth into `config/initializers/devise.rb`:

```ruby
config.omniauth :facebook,
                Rails.application.credentials.omniauth[:facebook][:key],
                Rails.application.credentials.omniauth[:facebook][:secret]

config.omniauth :google_oauth2,
                Rails.application.credentials.omniauth[:google][:key],
                Rails.application.credentials.omniauth[:google][:secret],
                scope: 'email, profile'
```

Since initializer is modified we need to restart server:

```
rails restart
```

Modify User model (`app/models/user.rb`) and say that logging from FB and G
are possible:

```ruby
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: [:facebook, :google_oauth2]

  def self.from_omniauth(auth)
    where(email: auth.info.email).first_or_create do |user|
      user.password = Devise.friendly_token[0, 20]
    end
  end
end
```

Add new sign in possibility into `app/views/welcome/index.html.haml`:

```haml
  = link_to "Sign in with Facebook", user_facebook_omniauth_authorize_path,
    class: 'btn btn-success'
  = link_to "Sign in with Google", user_google_oauth2_omniauth_authorize_path,
    class: 'btn btn-success'
```

Add routes configuration for starting FB and G conversation. Modify `config/routes.rb`:

```ruby
devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
```

and create devise omniauth controller. Add `app/controllers/users/omniauth_callbacks_controller.rb` with following content:

```ruby
class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def self.provides_callback_for(provider)
    class_eval %Q{
        def #{provider}
          auth = request.env["omniauth.auth"]
          if auth.info.email.blank?
            flash[:alert] = I18n.t('omniauth.email_missing',
                                   provider: "#{provider}".capitalize)
            redirect_to new_user_session_path
          else
            @user = User.from_omniauth(auth)
            if @user.persisted?
              sign_in_and_redirect @user, event: :authentication
              set_flash_message(:notice, :success, kind: "#{provider}".capitalize) if is_navigational_format?
            else
              session["devise.#{provider}_data"] = env["omniauth.auth"]
              redirect_to root_url
            end
          end
        end
    }
  end

  [:facebook, :google_oauth2].each do |provider|
    provides_callback_for provider
  end

  def failure
    redirect_to root_path
  end
end
```
* Explain secured credentials
* Mention about ruby metaprogramming and code generation

## Authorization using pundit

New requirements:
  * Only logged in users can create new article (already done thanks to Devise)
  * User owns article
  * Only article author can edit article
  * Published article cannot be edited or deleted
  * Other users see only published articles

### Relation between article and user

Add new db migration:

```
rails generate migration add_article_user
```

edit created migration:

```ruby
class AddArticleUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :articles, :user, index: true, foreign_key: true
  end
end
```

and define model relations

`app/models/user.rb`:
```ruby
class User < ApplicationRecord
  has_many :articles, dependent: :destroy
  ...
end
```

`app/models/article.rb`:
```ruby
class Article < ApplicationRecord
  belongs_to :user
  validates :user, presence: true
  ...
end
```

and edit `articles#create` action in `app/controlers/articles_controller.rb`
```ruby
def create
  @article = Article.new(article_params)
  @article.user = current_user
  ...
end
```

Show article author on

show (`app/views/articles/show.html.haml`)
```haml
%p
  %b Author email:
  = @article.user&.email
```

index (`app/views/articles/index.html.haml`)
```haml
%table
  %thead
    %tr
      %th Title
      %th Text
      %th Author email
      %th
      %th
      %th

  %tbody
    - @articles.each do |article|
      %tr
        %td= article.title
        %td= article.text
        %td= article.user&.email
        %td= link_to 'Show', article
        %td= link_to 'Edit', edit_article_path(article)
        %td= link_to 'Destroy', article, method: :delete, data: { confirm: 'Are you sure?' }

```

* Explain problem with existing articles and `edit` action and show potential
  solutions for this problem.
* Explain what does `&.` mean

### Article status

Create new db migration for article model:

```
rails g migration add_article_status
```

```ruby
class AddArticleStatus < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :status, :integer, null: false, default: 0
  end
end
```

and add possible statuses into `Article` model (`app/models/article.rb`)
```ruby
class Article < ApplicationRecord
  enum status: [:draft, :published]
  ...
end
```

run db migration:
```
rails db:migrate
```

To add possiblity to change status modify article form
(`app/views/articles/_form.html.haml`)
```haml
= simple_form_for @article do |f|
  = f.input :title
  = f.input :text
  = f.input :status, collection: Article.statuses.keys
  = f.button :submit
```

modify permitted article params (`app/controllers/articles_controller.rb`)
```ruby
def article_params
  params.require(:article).permit(:title, :text, :status)
end
```

and show article status on show (`app/views/articles/show.html.haml`):
```haml
%p
  %b Status:
  = @article.status
```

and index (`app/views/articles/index.html.haml`):
```haml
%table
  %thead
    %tr
      %th Title
      %th Text
      %th Author email
      %th Status
      %th
      %th
      %th

  %tbody
    - @articles.each do |article|
      %tr
        %td= article.title
        %td= article.text
        %td= article.user&.email
        %td= article.status
        %td= link_to 'Show', article
        %td= link_to 'Edit', edit_article_path(article)
        %td= link_to 'Destroy', article, method: :delete, data: { confirm: 'Are you sure?' }
```


Add pundit dependency into `Gemfile`:

```ruby
gem 'pundit'
```

run:

```
bundle install
rails generate pundit:install
rails restart
```

and add `Pundit` into `app/controllers/application_controller.rb`:
```ruby
class ApplicationController < ActionController::Base
  include Pundit
  before_action :authenticate_user!
end
```

create article policy `app/policies/article_policy.rb`:
```ruby
class ArticlePolicy < ApplicationPolicy
  def index?
    user
  end

  def create?
    user
  end

  def show?
    record.user == user || record.published?
  end

  def edit?
    record.user == user && record.draft?
  end

  def update?
    record.user == user && record.draft?
  end

  def destroy?
    record.user == user && record.draft?
  end
end
```

* Show what will happened when trying to edit not owned article

add not authorized error handler (`app/controllers/application_controller.rb`):
```ruby
rescue_from Pundit::NotAuthorizedError do |exception|
  redirect_back fallback_location: root_path,
                alert: 'Permission denied'
end
```

show Show, Edit and Destroy article links only when user is permitted to do so
(`app/views/articles/index.html.haml`):
```haml
%tbody
  - @articles.each do |article|
    %tr
      %td= article.title
      %td= article.text
      %td= article.user&.email
      %td= article.status
      %td
        - if policy(article).show?
          = link_to 'Show', article
      %td
        - if policy(article).edit?
          = link_to 'Edit', edit_article_path(article)
      %td
        - if policy(article).destroy?
          = link_to 'Destroy', article, method: :delete, data: { confirm: 'Are you sure?' }
```

Last requirement is to see only owned or published articles:

Modify `app/policies/article_policy.rb` and add:
```ruby
class ArticlePolicy < ApplicationPolicy
  class Scope
    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      @scope.where(status: :published).or(Article.where(user: @user))
    end
  end
  ...
end
```

and change Article controller (`app/controllers/article_controller.rb`):
```ruby
def index
  authorize Article
  @articles = policy_scope(Article)
end
```
